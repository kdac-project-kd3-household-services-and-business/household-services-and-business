#### Household Services And Business

### Description
It is the platform that connect with the consumers and professional startup,
small businesses service providers. In this, users can search the services
based on Category,Location,VisitCharges and service providers can list
out their services, budget, and locations so that users can easily find the
services at nearby areas.Due to easy convenience, people search the online
services and call to the service providers at their doorsteps.This applications
provide the one-stop solutions for local Service Providers and consumers to deal with
the household services at one place.

### Functionality
Service for household and business is an application that provides services
by just clicking and selecting on the require service. To get the services
from it you need to post the service required and the service providers you
can select it and you can set an appointment with them.

### User(Admin/Servicemen/Customer)
- Register
- Login
- Update Profile
- Reset Password
- View Dashboard
- Signout

## Admin 
- City Management(CityID,City,Pincode,State,Lang.&Long)
- Activate/Deactivate logins of Customer/Serviceman
- Verify Serviceman's Profile(Address Proof, ID Proof,Other Details)
- ServiceCategory Management(CategoryId,Category,Description)
- Verify Feedbacks

## Serviceman
- Update Personal Details 
- Update Visiting Charges
- Post Feedback To Customer(Rating,Comment)
- Work Gallery(Photo/Video,CustID,ServicemanID,Caption)
- View Feedback
- View Request List(Accept/Reject/Reschedule)
 
## Customer
- Update Personal Details 
- Post Feedback To Serviceman(Rating,Comment)
- Search Serviceman by Category,Location,VisitCharges(Filter)
- View Serviceman's Profile(Name,Address,Charges,Reviews)
- Send Request 
- Checkout
 
## References
 [USE CASE Diagram](https://gitlab.com/kdac-project-kd3-household-services-and-business/household-services-and-business/-/blob/master/Usecase.png)